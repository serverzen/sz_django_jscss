=======
Changes
=======

0.6 - unreleased
================

  * based on Twitter Bootstrap 2.3.1

  * includes JQuery 1.9.1

  * first release


