from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read().strip()

with open('CHANGES.rst') as f:
    changes = f.read().strip()


setup(name='sz_django_jscss',
      version='0.6',
      description='Django app with common javascript/css libraries',
      long_description=readme + '\n\n' + changes,
      classifiers=["Programming Language :: Python"],
      author='Rocky Burt',
      author_email='rocky@serverzen.com',
      url='http://pypi.python.org/pypi/sz_django_jscss',
      keywords='django serverzen bootstrap jquery',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=True,
      install_requires=['django'],
      entry_points='',
      )
