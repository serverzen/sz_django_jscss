from django.conf.urls import patterns, url


urlpatterns = patterns(
    'sz_django_jscss.views',
    url(r'^sz_django_jscss/test$', 'test_js', name='test_js'),
)
