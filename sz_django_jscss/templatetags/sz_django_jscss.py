from django import template
from django.conf import settings

register = template.Library()


CSS = {
    'bootstrap': [u'bootstrap/css/bootstrap.min.css'],
}

JS = {
    'bootstrap': [u'bootstrap/js/bootstrap.min.js'],
    'jquery': [u'jquery/jquery-1.9.1.min.js'],
    'knockout': [u'knockout/knockout-2.2.1.js'],
    'requirejs': [u'requirejs/require.js'],
}


@register.simple_tag(takes_context=True)
def css(context, library):
    res = u''
    for item in CSS[library]:
        res += (u'<link href="%ssz_django_jscss/%s"'
                u' rel="stylesheet" media="screen"/>'
                % (settings.STATIC_URL, item))
    return res


@register.simple_tag(takes_context=True)
def js(context, library):
    res = u''
    for item in JS[library]:
        res += (u'<script src="%ssz_django_jscss/%s"></script>'
                % (settings.STATIC_URL, item))
    return res
