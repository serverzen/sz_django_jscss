from django.shortcuts import render_to_response
from django.template import RequestContext


def test_js(request):
    return render_to_response('sz_django_jscss/test-js.html',
                              context_instance=RequestContext(request))
